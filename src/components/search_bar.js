import React, { Component, PropTypes } from 'react';


class SearchBar extends Component {
    constructor(props) {
        super(props);

        this.state = { term: '' };
    }
    onInputChange(term) {
        this.setState({ term }); // eslint-disable-line react/no-set-state
        this.props.onSearchTermChange(term);
    }
    render() {
        return (
            <div className={"search-bar"}>
                <input
                    onChange={event => this.onInputChange(event.target.value)}
                    value={this.state.term}
                />
            </div>
        );
    }
}

SearchBar.propTypes = {
    onSearchTermChange: PropTypes.function.isRequired,
};

export default SearchBar;
