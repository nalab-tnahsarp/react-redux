import _ from 'lodash';
import React, { Component } from 'react';
import ReactDom from 'react-dom';
import SearchBar from './components/search_bar';
import VideoList from './components/video_list';
import VideoDetail from './components/video_details';
import YTSearch from 'youtube-api-search';
const ApiKey = 'AIzaSyCi69jVlv8Cm56Chl_TlbQMVi-zB3N3bbI';


class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedVideo: null,
            videos: [],
        };

        this.videoSearch('surfboards');
    }

        videoSearch(term) {
            YTSearch({ key: ApiKey, term }, (videos) => {
                this.setState({ // eslint-disable-line react/no-set-state
                    selectedVideo: videos[0],
                    videos,
                });
            });
        }
    render() {
        // TODO: remove unused constant
        const videosearch = _.debounce((term) => { this.videoSearch(term); }, 300);

        return (
            <div>
                <SearchBar onSearchTermChange={term => this.videoSearch(term)} />
                <VideoDetail video={this.state.selectedVideo} />
                <VideoList
                    onVideoSelect={selectedVideo => this.setState({ selectedVideo })} // eslint-disable-line react/no-set-state
                    videos={this.state.videos}
                />

            </div>
        );
    }
}


ReactDom.render(<App />, document.querySelector('.container'));
